# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

    user = User.create(username: 'Mike' , email: 's3600000@student.rmit.edu.au' , password: 'FooBar55$$**')
    user1 = User.create(username: 'Jane' , email: 's3600001@student.rmit.edu.au' , password: 'FooBar55$$**')
    user2 = User.create(username: 'Tu'  , email: 's3600002@student.rmit.edu.au' , password:  'FooBar55$$**')
    user3 = User.create(username: 'Jake' , email: 's3600003@student.rmit.edu.au', password:  'FooBar55$$**')
    
    post0 = Post.create(title: "Safe ways to do things in bash" , source: '' , user_id: user.id )
    post1 = Post.create(title: "Archive of Operating Systems" , source: 'https://archiveos.org' , user_id: user1.id )
    post2 = Post.create(title: "Pure CSS Francine" , source: 'https://archaeology.org' , user_id: user2.id )
    post3 = Post.create(title: "I Don’t Know How to Waste Time on the Internet Anymore " , source: 'https://nymag.com' , user_id: user.id )
    post4 = Post.create(title: "GHC Parser Principles" , source: '' , user_id: user.id )
    post5 = Post.create(title: "John Carmack: My Steve Jobs Stories" , source: 'https://theguardian.com' , user_id: user3.id )
    post6 = Post.create(title: "The trade in fraudulently obtained airline tickets" , source: '' , user_id: user2.id )
    post7 = Post.create(title: "	Is Prefix of String in Table? a Journey into SIMD String Processing" , source: '' , user_id: user1.id )
    post8 = Post.create(title: "Does Clean Laundry Have Germs?" , source: 'https://stopthestomachflu.com' , user_id: user3.id )
    
    post9 = Post.create(title: "Is Prefix of String in Table? a Journey into SIMD String Processing" , source: 'https://trent.me' , user_id: user1.id )
    post01 = Post.create(title: "Copyright law hides work like Zora Neale Hurston's newly published first book" , source: 'https://espn.com' , user_id: user2.id )
    post02 = Post.create(title: "Favstar will shut down as a result of Twitter’s API changes for data streams " , source: 'https://techcrunch.com' , user_id: user.id )
    post03 = Post.create(title: "Gmail Icons are Hard" , source: 'https://github.com' , user_id: user.id )
    post04 = Post.create(title: "Manhattan Project: Australian physicist alerted Britain to US secrecy plan" , source: 'https://nytimes.com' , user_id: user3.id )
    post05 = Post.create(title: "Enslaved and then marooned on Tromelin Island for fifteen years (2014)" , source: 'https://latimes.com' , user_id: user1.id )
    post06 = Post.create(title: "Gödel and the unreality of time" , source: 'https://blog.github.com' , user_id: user2.id )
    post07 = Post.create(title: "Linux ate my RAM (2009)" , source: 'https://nus.edu.sg' , user_id: user.id )
    
    comment = Comment.create(user_id: user.id , post_id: post0.id , content: "yeah I agree")
    comment1 = Comment.create(user_id: user2.id , post_id: post1.id , content: "interesting" )
    comment2 = Comment.create(user_id: user3.id , post_id: post0.id , content: "cool" )
    comment3 = Comment.create(user_id: user1.id , post_id: post3.id , content: "O M G" )
    comment4 = Comment.create(user_id: user3.id , post_id: post03.id , content: "unbelievable")
    comment5 = Comment.create(user_id: user2.id , post_id: post04.id, content: "not surprised" )
    comment6 = Comment.create(user_id: user1.id , post_id:  post05.id, content: "hmmmmmmmmmmmm")
    comment7 = Comment.create(user_id: user.id , post_id:  post06.id, content: "XDXD")
    comment8 = Comment.create(user_id: user.id , post_id: post02.id, content: "postgres is cool")
    comment9 = Comment.create(user_id: user3.id , post_id: post3.id, content: "university")
    comment01 = Comment.create(user_id: user2.id , post_id: post8.id, content: "artificial intelligence")
    comment02 = Comment.create(user_id: user2.id , post_id: post8.id, content: "yeah I agree")
    comment03 = Comment.create(user_id: user1.id , post_id: post8.id, content: "cool")
    comment04 = Comment.create(user_id: user1.id , post_id: post8.id, content: "does it have germs")
    
    
    
    
