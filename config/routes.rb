Rails.application.routes.draw do
  

  root 'posts#index'
  
  resources :sessions, only: [:new, :create, :destroy]
  get '/home' , to: 'posts#index'
  #get '/home', to: 'static_pages#home'
  get '/newcomments', to: 'static_pages#comments'

  post '/addnewcomment' , to: 'comments#create'
  post '/addnewpost' , to: 'posts#create'
  get '/submit', to: 'static_pages#submit'
  get '/login' , to: 'sessions#new'
  get '/signup' , to: 'users#new'
  get '/about' , to: 'static_pages#about'
  get '/api' , to: 'static_pages#api'
  get '/new' , to: 'users#new'
  get 'login' , to: 'sessions#new'
  post '/login' , to: 'sessions#create'
  
  delete '/logout' , to: 'sessions#destroy'
  match '/signout',  to: 'sessions#destroy', via: :delete
  post "users/:id/posts/:id"    => "comments#create"
  resources :users
  
  
  get '/create' , to: 'posts#create' , via: [:get, :post]
  get '/createcomment' , to: 'comments#new' , via: [:get, :post]
  
  post '/v0/item/create' , to: 'items#create'
  get '/v0/item' , to: 'items#index' , :defaults => { :format => 'json' } 
  get '/v0/item/:id' , to: 'items#show' ,  :defaults => { :format => 'json' } 
  
  resources :users do
  resources :posts
  end
  
  resources :posts, only: [:new, :create, :index] 
  resources :items, only: [:index , :show] , :defaults => { :format => 'json' } 
end
