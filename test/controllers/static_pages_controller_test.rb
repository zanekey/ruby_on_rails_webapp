require 'test_helper'



class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  #test the correct pages are retrieved
  
  test "should get home page" do
    get home_path
    assert_response :success
    # add an assertion to check page title is correct
  end
  
  test "should get comments page" do
    get newcomments_path
    assert_response :success
  end
  
  test "should get login page do" do
    get login_path
    assert_response :success
  end
  
  test "should get api page" do
    get api_path
    assert_response :success
  end
  
  test "should get about page" do
    get about_path
    assert_response :success
  end
  
  
end
