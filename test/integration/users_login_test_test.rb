require 'test_helper'

class UsersLoginTestTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "invalid signup attribute information provided" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { username:  "",
                                         email: "student@invalid",
                                         password:              "invalid",
                                         password_confirmation: "wrong" } }
    end
    assert_template 'users/new'
  end
  
  test "valid signup attribute information provided" do
    get signup_path
    assert_difference 'User.count' , 1 do
      post users_path, params: { user: { username:  "valid input",
                                         email: "s3607212@student.rmit.edu.au",
                                         password:              "FooBar55**$$",
                                         password_confirmation: "FooBar55**$$" } }
    end
    follow_redirect!
    assert_template 'users/show'
  end
  
end
