require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  
  def setup
    user = User.new( username: "valid title"  , email: "s3612354@student.rmit.edu.au" , password: "FooBar55**$$")
    post = user.posts.new( user_id: user.id ,  title: "valid title name" , source: "https://google.com" )
    user.save
    post.save
    @comment = Comment.new(content: "hello" , user_id: user.id , post_id: post.id)
  end

  test 'valid comment' do
    assert @comment.valid?
  end
  
  test 'invalid comment' do
    @comment.content = 'ss'
    assert_not @comment.valid?
  end
  
  test 'comments for unsaved users' do
    user = User.new( username: "valid title"  , email: "s3612354@student.rmit.edu.au" , password: "FooBar55**$$")
    post = user.posts.new( user_id: user.id ,  title: "valid title name" , source: "https://google.com" )
    post.save
    testComment = Comment.new(content: "hello" , user_id: user.id , post_id: post.id)
    assert_not testComment.valid?
  end
  
   test 'comments for unsaved posts' do
    user = User.new( username: "valid title"  , email: "s3612354@student.rmit.edu.au" , password: "FooBar55**$$")
    post = user.posts.new( user_id: user.id ,  title: "valid title name" , source: "https://google.com" )
    user.save
    testComment = Comment.new(content: "hello" , user_id: user.id , post_id: post.id)
    assert_not testComment.valid?
  end
  
end
