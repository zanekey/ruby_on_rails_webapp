require 'test_helper'

class UserTest < ActiveSupport::TestCase

   def setup
    @user = User.new( username: "valid"  , email: "s3612354@student.rmit.edu.au" , password: "FooBar55**$$")
  end
  
  test 'valid user' do
    assert @user.valid?
  end
  
  test 'no email provided' do
    @user.email = nil
    assert_not @user.valid?
  end
  
  test 'invalid email provided' do
    @user.email = "invalid@invalid"
    assert_not @user.valid?
  end
  
  test 'invalid password provided' do
    @user.password = "invalid"
    assert_not @user.valid?
  end
  
  test 'correct password provided' do
    @user.password = "FooBar55**$$"
    assert @user.valid?
  end
  
  test 'invalid username provided' do
    @user.password = "a"
    assert_not @user.valid?
  end
  
  test 'valid username provided' do
    @user.username = "valid"
    assert @user.valid?
  end
  
end
