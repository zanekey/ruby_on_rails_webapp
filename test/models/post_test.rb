require 'test_helper'

class PostTest < ActiveSupport::TestCase
  
  def setup
    user = User.new( username: "valid title provided"  , email: "s3612354@student.rmit.edu.au" , password: "FooBar55**$$")
    @post = user.posts.new( user_id: user.id ,  title: "valid title name" , source: "https://google.com" )
  end
  
  test 'valid post' do
    assert @post.valid?
  end
  
  test 'empty source is valid' do
    @post.source = nil
    assert @post.valid?
  end
  
  test 'invalid source' do
    @post.source = 'invalid source format'
    assert_not @post.valid?
  end
  
  test 'blank title' do
    @post.title = nil
    assert_not @post.valid?
  end
  
  test 'short title' do
    @post.title = 'short'
    assert_not @post.valid?
  end
  
end
