class Comment < ApplicationRecord
  belongs_to :post
  belongs_to :user
  
  
  COMMENT_REGEX = /(.*[\S]){3}/
  validates :content , presence: true , format: { with: COMMENT_REGEX } , length: { minimum: 3 , maximum:1000}
end
