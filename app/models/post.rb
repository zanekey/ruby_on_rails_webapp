require 'open-uri'
class Post < ApplicationRecord
  
  has_many :comments
  belongs_to :user
  
  URL_REGEXP = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/
  validates :title , presence: true , length: { minimum: 10 , maximum: 200 , message: " title must be between 10 and 200 charachters long" }
  validates :source , :allow_blank => true , format: { with: URL_REGEXP }

  
end
