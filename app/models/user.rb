class User < ApplicationRecord
   has_many :comments
   has_many :posts
   #regex for emailformat
   #((s|S)[\d]{7}[@]{1}(student\.|)(rmit\.edu\.au))
    
    before_save { self.email = email.downcase }
    
    EMAIL_REGEX = /((s|S)[\d]{7}[@]{1}(student\.|)(rmit\.edu\.au))/
    USERNAME_REGEX = /[\w\-]/
    PASSWORD_REGEX = /(.*[a-z]){1,}(.*[A-Z]){1,}(.*[\d]){1,}(.*[\^$.|?*+(){}]){1,}([\S]){1,}/
    validates :username , presence: true , length: { minimum: 2 , maximum: 15} , format: { with: USERNAME_REGEX } , uniqueness: {case_sensitive: true}
    validates :email , presence: true , length: {maximum: 255} , format: { with: EMAIL_REGEX } ,  uniqueness: {case_sensitive: false , message: "Email must be a valid rmit email address" }
    
    has_secure_password
    validates :password , presence: true ,  length: { minimum: 10 } , format: { with: PASSWORD_REGEX ,  message: "must contain at least one uppercase letter, one special character (\^$.|?*+(){}), one number and
        one lowercase letter" } 
end
