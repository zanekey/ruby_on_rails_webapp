json.partial! "items/item", item: @item

    
    tempItem = @item
    
    if tempItem == nil
        json.error "Item Not Available"
    else
    
        if tempItem.id % 2 == 0
            json.id tempItem.id
            json.type "post"
            json.by User.find_by(id: tempItem.user_id).username
            json.time tempItem.created_at
            json.url tempItem.source
            json.title tempItem.title
        else
            json.id tempItem.id
            json.type "post"
            json.by User.find_by(id: tempItem.user_id).username
            json.time tempItem.created_at
            json.text tempItem.content
        end
    end
