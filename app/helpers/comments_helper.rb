module CommentsHelper
    
  #retrieve all comments
  def get_all_comments
    return @comments = Comment.all.order(created_at: :desc)
  end
  
  # retrieve the amount of comments for a particular post
  def get_number_of_comments_for(post)
    return post.comments.count
  end
    
end
