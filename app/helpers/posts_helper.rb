module PostsHelper
    
    COMMENTS_PER_PAGE = 3
    def getAllPosts
        return Post.all
    end
    
    def get_number_of_comments
        Post.find( params[:id]).comments.count
    end
    
    def get_number_of_posts
        return Post.all.count
    end
    
    def get_latest_three_comments
        Post.find( params[:id]).comments.order(created_at: :desc).limit(COMMENTS_PER_PAGE)
       
    end

end
