class StaticPagesController < ApplicationController
    
    #define main pages
    def home
        
    end
    
    def comments
    end

    #if user is not logged in redirect to login_path
    def submit
      
        if !logged_in?
            redirect_to login_path
        else
            @post = Post.new
        end
    end
    
    def login
    end
    
    def about
    end
    
    def api
    end


    
end
