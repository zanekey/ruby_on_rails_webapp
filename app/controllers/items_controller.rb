class ItemsController < ApiController
    
    # call actions before the specified actions are called
    before_action :instantiate , only: [:index , :show]
    
    #method to retrun all the posts available in db
    def get_all_posts
        postsArray = Array.new
        newID = 0
        Post.all.each do |post|
            post.id = newID
            postsArray << post
            newID += 2
        end
        return postsArray
    end
    
    #method to return all the comments available in db
    def get_all_comments
        commentsArray = Array.new 
        newID = 1
        Comment.all.each do |comment|
            comment.id = newID
            commentsArray << comment
            newID += 2
        end
    return commentsArray
    end
    
    #the index action , returns all available items ( posts and comments)
    def index
       @items = get_all_posts + get_all_comments
    end
    
    # show individual items
    def show
       @items = get_all_posts + get_all_comments
        @items.each do |item|
            if item.id == params['id'].to_i
            @item = item
            return @item
            end
        end
    end
    
    #create items 
    def create
        userID = User.find_by(username: params[:by]).id
        #check type of POST
        if params[:type].downcase == 'post'
            @item = Post.new( { :id => params[:id] , :user_id => userID , :title => params[:title] , :source => [:url]} )
        elsif params[:type].downcase == 'comment'
            @item = Comment.new( { :id => params[:id] , :content => params[:text] , :post_id => params[:post_id] , :user_id => userID} )
        end
    end
    
    # instantiate post and comment arrays
    def instantiate
       get_all_posts
       get_all_comments
    end

    
end
