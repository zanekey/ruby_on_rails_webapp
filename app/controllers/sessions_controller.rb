class SessionsController < ApplicationController
    
  def new
  end
  
  def create
    user = User.find_by(username: params[:session][:username] )
    if user && user.authenticate(params[:session][:password])
      #log the user in and redirect to the user's show page
      flash[:success] = "Login Successful"
      log_in(user)
      redirect_to user
    else  
      flash.now[:danger] = 'Invalid /username/password combination'
      render 'new' 
    end
  end
  
  def destroy
    log_out
    redirect_to root_path
  end
end
