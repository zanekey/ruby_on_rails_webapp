class PostsController < ApplicationController
    #declare constants
    INCREMENT_AMOUNT = 8
    
    #set  before  actions
    before_action :set_page, only: [:index]
     
    #action for showing posts pages
    def show
        post = Post.find_by(id: params[:id])
        #@comment = post.comments
        @newcomment = Comment.new
        @newcomment.post = post
    end
    
    #indexing  of  pagination
    def index
        @posts = Post.order(created_at: :desc).limit(INCREMENT_AMOUNT).offset(@page * INCREMENT_AMOUNT)
    end
   
    #new post
    def new
        before_filter :authenticate_user!
        @post = Post.new
        @user = current_user
    end
    
    #creating post
    def create
        @current_user||= User.find_by(id: session[:user_id])
        @post = @current_user.posts.new(post_params)
        @post.user_id = current_user.id
        #process saving post and display appropiate messages  accordingly
        if @post.save
            flash[:success] = " News posted successfully"
            redirect_to :root
        else
            #redirect back to submit page
            render 'static_pages/submit'
        end
    end
    
    #retrieve all comments 
    def get_all_comments_for_post
        @post = Post.find( params[:id]).comments
    end
    
    private
        #set page  number
        def set_page
            @page = params[:page].to_i || 0
        end
        
        #post params
        def post_params
          params.require(:post).permit(:title, :source)
        end
end
