class CommentsController < ApplicationController
  
  def new
    @comment = Comment.new
  end
  
  #creating new comment
  def create
    @comment = Comment.new(comment_params)
    @comment.user_id = current_user.id
    
    #if user is saved , process banner message and redirect accordinly
    if @comment.save
      flash[:success] = "Comment posted Successfully"
      redirect_to root_path
    else
      flash[:error] =  "comment content must not be blank and be between 3 and 1000 charachters long excluding any whitespace charachters" 
      redirect_to user_post_path(user_id: current_user, id: @comment.post)
    end
  end
  
  private
    def comment_params
      params.require(:comment).permit(:content , :post_id)
    end
end
