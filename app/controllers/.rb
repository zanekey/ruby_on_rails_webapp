class ItemsController < ApplicationController
  def type:string
  end

  def by:string
  end

  def time:datetime
  end

  def text:string
  end

  def url:string
  end

  def title:string
  end
end
